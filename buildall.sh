#!/bin/bash

echo "Cleaning up from last session..."
docker-compose down
rm -rf postgresdata
docker container prune -f
docker image prune -f
echo ""

echo "Building main nominatim container..."
docker build -t nominatim nominatim
echo ""

echo "Building postgres container..."
docker build -t nominatim-postgres postgres
